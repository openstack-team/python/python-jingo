python-jingo (0.9-2) UNRELEASED; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Ondřej Nový ]
  * d/watch: Fixed tags with "v" prefix
  * d/p/new_django_http_status_code.patch: Fix HTTP status code for new Django
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Add trailing tilde to min version depend to allow
    backports
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * Use debhelper-compat instead of debian/compat.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Fri, 04 Aug 2017 21:59:18 +0200

python-jingo (0.9-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * New upstream release.
  * Rebased patches.
  * Rename patches so it's less messy.
  * Add patch (Closes: #828675):
    - add-django.contrib.sites-to-INSTALLED_APPS.patch
    - django-1.10-defines_TEMPLATES.patch
    - django-1.10-read-debug-boolean-from-template-backend.patch
    - django-1.10-django.forms.util-renamed.patch
    - django-1.10-Loader-class-__init__-takes-2-args.patch
    - django-1.10-urls.patterns-removed.patch
  * Fixed debian/copyright ordering.
  * Standards-Version is now 3.9.8 (no change).

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Jul 2016 09:31:10 +0000

python-jingo (0.8-1) unstable; urgency=medium

  * New upstream release.
  * Removed django_1.8_compatability_fixes.patch applied upstream.
  * Added fix-removed-TemplateDoesNotExist-from-django-1.9.patch
    (Closes: #806365).
  * Added fix-django.utils.importlib-removed-from-django-1.9.patch.
  * Added add-django.contrib.sites-to-installed-apps.patch.
  * Added remove-broken-django-tests.patch.
  * Fix run_tests.py for Django 1.9.
  * Moved some build-depends in build-depends-indep, added sqlite3 as
    build-depends-indep.
  * Added use-six-not-from-django.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Dec 2015 10:04:05 +0000

python-jingo (0.7.1-2) unstable; urgency=medium

  * Fix FTBFS (Closes: #806365).

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Dec 2015 09:44:15 +0000

python-jingo (0.7.1-1) unstable; urgency=medium

  * New upstream release.
  * Removed defines-own-StrAndUnicode.patch applied upstream.
  * Added django_1.8_compatability_fixes.patch from upstream, to fix a Django
    1.8 compat (Closes: #801934).
  * Refreshed the other 2 patches.
  * Watch file now using github tag and not PyPi.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Oct 2015 11:46:10 +0200

python-jingo (0.7-2) unstable; urgency=medium

  * Added fix-run_tests.py-with-django-1.7.patch (Closes: #765160).
  * Added defines-own-StrAndUnicode.patch.
  * Standards-Version is now 3.9.6 (no change).

 -- Thomas Goirand <zigo@debian.org>  Fri, 17 Oct 2014 08:08:54 +0000

python-jingo (0.7-1) unstable; urgency=medium

  * Initial release. (Closes: #760862)

 -- Thomas Goirand <zigo@debian.org>  Tue, 12 Aug 2014 22:39:45 +0800
